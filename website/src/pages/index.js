import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
  {
    title: 'DevOps advocate',
    imageUrl: 'img/undraw_docusaurus_mountain.svg',
    description: (
      <>
        The DevOps process flow is all about agility and automation. Each
        phase in the DevOps lifecycle focuses on closing the loop between
        development and operations and driving production through continuous
        development, integration, testing, monitoring and feedback, delivery, and deployment.
      </>
    ),
  },
  {
    title: 'Automate Everything',
    imageUrl: 'img/undraw_docusaurus_tree.svg',
    description: (
      <>
        There are so many tools out there that many claim to be the silver bullet.
        This site will have some notes for leveraging these tools and some
        opinions on what does and doesn't work. The unfortunate truth is that
        no tool is a "one size fits all" and will need adaptations.
      </>
    ),
  },
];

function Feature({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={clsx('col col--6', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

export default function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`Automate Everything ${siteConfig.title}`}
      description="Description will go into a meta tag in <head />">
      <header className="header">
        <div className="container">
          <h1 align="center" className="hero__title">{siteConfig.title}</h1>
          <p align="center" className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={clsx(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/')}>
              View my Docs
            </Link>
            &nbsp;
            <Link
              className={clsx(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('mattwilce-cv')}>
              View my CV
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}
