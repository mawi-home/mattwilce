﻿**MATT WILCE** Kent UK
<matt.wilce+cv@gmail.com>        <https://www.linkedin.com/in/mattwilce>

I am a confident, reliable and diligent worker with over 15 years experience within the public and private IT sectors. I am line manager to a team of T shaped engineers and run an agile scrumban as well as providing their performance reviews and creating PDP’s. I regularly liaise with all levels of business, both internally and with customers and orchestrate meetings to ensure the backlog is prioritised and aligned to the business requirements.

**CAREER SUMMARY**

I started off by learning how to program on a ZX Spectrum in the early 90’s and my interest developed from there into building computers and networking. I continue to have various personal projects on the go with Arduino and Raspberry Pi as well as 3d printing and car tuning software. In my career I started off with data entry and have worked through systems support, sysadmin, network admin, consultant and into DevOps where I continue to seek out the latest technologies and to expand my skillset.

Some highlights are below:

- I lead and mentor teams internally and with clients with a mixture of developers and platform engineers. Following ITIL and agile approaches to ensure global standards are maintained. Running standups, sprint planning/reviews and knowledge sharing
- I have in depth knowledge with the 3 big cloud providers having worked with AWS for 12 years, Azure for 7 years and GCP for 4 years and now automate hybrid deployments and migrations from bare metal
- Extensive Linux and Unix experience starting with Ubuntu 6 (in 2006) and worked with Redhat, Centos, SunOS and more recently containers such as Alpine and Debian Stretch
- Many years of experience with deploying, maintaining and developing schemas for databases and event streams including Couchdb, Kafka, MongoDB, MySQL, Neo4j, PostgreSQL, Pulsar and RabbitMQ
- For the last 6 years I have been heavily focussed on automating everything with Ansible, Terraform and Chef with dynamic, reusable code which makes up the product
- Metrics and log gathering using tools such as Elasticsearch, Fluentd, Grafana, Logstash, Loki, Kibana, Nagios, and Prometheus, Splunk and Zabbix
- Python programming for things such as custom Ansible modules and scripts for semantic versioning for a release process
- Worked on building, deploying, modifying and maintaining MEAN and MERN stacks and most recently Go with chirpstack and custom Pulsar functions
- Nearly 7 years of experience with Kubernetes for production services using cloud native solutions, AKS, EKS and GCP; as well as RKE and minikube for bare metal and local installations. Recently including service mesh for federated clusters
- 8 years of experience with building and deploying docker images with docker swarm and compose
- 11 years of designing, managing and maintaining CICD pipelines with tools such as Jenkins, Bitbucket, Github, Gitlab and Concourse


**MAY 2017 - PRESENT**

**DEVOPS LEAD** PENTA TECHNOLOGY

Responsible for managing the product, development teams and providing consultancy to customers.  Focus on designing, automating and monitoring the build and deployment of high quality, production ready systems and services while ensuring the confidentiality, integrity and availability of all data. The product is an accelerator tool for deploying blueprinted architecture into AWS, Azure, GCP and bare-metal with focus on Kubernetes.

**MAY 2016 – MAY 2017**

**SENIOR LINUX SYSTEMS ADMINISTRATOR** DOVETAIL SERVICES

My primary role was 3rd line support of the company’s global systems and services. This included implementing and overhaul of the monitoring and alerts to maintain system performance and uptime. I was made redundant from this position when they closed down.

**JAN 2015 – MAY 2016**

**TECHNICAL CONSULTANT** AMIDO

My primary role was the management of internal technical operations. This included implementing scalable solutions for internal processes and procurement of software and hardware. I left this position to seek a product supporting role.

**MAY 2011 – DEC 2015**

**INFRASTRUCTURE & SUPPORT** MOBBU

My primary role was supporting the middleware and mobile applications deployed nationwide and managing 500+ devices in BES. I was made redundant from this position when they closed down.

**COURSES & QUALIFICATIONS**

| Courses | Qualifications |
| :---: | :---: |
| K8s & cloud native associate course | CompTIA Security+ |
| Advanced terraform with azure course | ITIL foundation |
| Google associate cloud engineer course | CompTIA Network+ |
| Kubernetes Administrator course | Police vetted to NPPV3 |
| AWS DevOps Engineer course | Level 3 PAT |
| Ansible best practices course | AVCE ICT double award |

**INTERESTS**

I enjoy mentoring my staff as it’s great to see when things start to “click”

In my own time I like to tinker with various engineering and motorsport projects like custom sensors with micro controllers and have also attained my ARDS racing licence.

In my down time I like to get back to nature with my wife and often seek out obscure holidays like staying in tree houses and beach huts.
